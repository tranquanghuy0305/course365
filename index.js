const express = require("express"); // Tương tự : import express from "express";
const path = require("path");
const cors = require('cors');
const serverless = require("serverless-http");
//import thư viện mongoose
const mongoose = require("mongoose");
// Khởi tạo Express App
const app = express();
app.use(express.json());
app.use(cors());
//kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_COURSE365")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));
//khai báo các model
const courseModel = require('./app/models/course365.model');
//khai bao port
const port = 8000;
//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/views"));

app.use((request, response, next) => {
  var today = new Date();

  console.log("Current time: ", today);

  next();
});

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/index.html"));
})

app.get("/admin",(request, response) => {
  console.log(__dirname);
  response.sendFile(path.join(__dirname + "/views/admin/admin.html"));
})
//khai báo routers
const courseRouter = require("./app/routes/course365.router");


app.use("/api/v1/courses", courseRouter);
app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
module.exports = app;
module.exports.handler = serverless(app);
