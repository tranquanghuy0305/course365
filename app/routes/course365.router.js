const express = require("express");

const courseController = require("../controllers/course365.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/", courseController.getAllCourse)

router.post("/", courseController.createCourse);

router.get("/:courseId", courseController.getCourseById);

router.put("/:courseId", courseController.updateCourse);

router.delete("/:courseId", courseController.deleteCourse);

module.exports = router;
