//b1 khai báo thư viện mongoose
const mongoose = require("mongoose");

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const courseSchema = new Schema({
    id: mongoose.Types.ObjectId,
    courseCode: {
        type: String,
        require: true,
        unique:true
    },
    courseName: {
        type: String,
        require: true  
    },
    price: {
        type: Number,
        require: true
    },
    discountPrice : { 
        type: Number,
        require:true
    },
    duration: {
        type: String,
        require:true,
    },
    level: {
        type:String,
        require:true
    },
    coverImage: {
        type:String,
        require:true,
    },
    teacherName : {
        type:String,
        require:true,
    },
    teacherPhoto : {
        type:String,
        require:true,
    },
    isPopular: {
        type:Boolean,
        default:true,
    },
    isTrending: {
        type:Boolean,
        default:false,
    }

    
});

//b4 biên dịch schema thành model
module.exports = mongoose.model("course",courseSchema)